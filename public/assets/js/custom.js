$(document).ready(function(){
  //intialization
  $('#interview_experience_salary_div').hide();
  $('#interview_custom_pf_div').hide();


  //var mem1total = 0;
  //interview total marks panel 1
  $( "#mem1vc" ).change(function() {
      var mem1total = Number($('#mem1vc').val()) + Number($('#mem1la').val()) + Number($('#mem1is').val()) + Number($('#mem1i').val());
      $('#mem1total').val(mem1total);
  });
  $( "#mem1la" ).change(function() {
      var mem1total = Number($('#mem1vc').val()) + Number($('#mem1la').val()) + Number($('#mem1is').val()) + Number($('#mem1i').val());
      $('#mem1total').val(mem1total);
  });
  $( "#mem1is" ).change(function() {
      var mem1total = Number($('#mem1vc').val()) + Number($('#mem1la').val()) + Number($('#mem1is').val()) + Number($('#mem1i').val());
      $('#mem1total').val(mem1total);
  });
  $( "#mem1i" ).change(function() {
      var mem1total = Number($('#mem1vc').val()) + Number($('#mem1la').val()) + Number($('#mem1is').val()) + Number($('#mem1i').val());
      $('#mem1total').val(mem1total);
  });

  //interview total marks panel 2
  $( "#mem2vc" ).change(function() {
      var mem2total = Number($('#mem2vc').val()) + Number($('#mem2la').val()) + Number($('#mem2is').val()) + Number($('#mem2i').val());
      $('#mem2total').val(mem2total);
  });
  $( "#mem2la" ).change(function() {
      var mem2total = Number($('#mem2vc').val()) + Number($('#mem2la').val()) + Number($('#mem2is').val()) + Number($('#mem2i').val());
      $('#mem2total').val(mem2total);
  });
  $( "#mem2is" ).change(function() {
      var mem2total = Number($('#mem2vc').val()) + Number($('#mem2la').val()) + Number($('#mem2is').val()) + Number($('#mem2i').val());
      $('#mem2total').val(mem2total);
  });
  $( "#mem2i" ).change(function() {
      var mem2total = Number($('#mem2vc').val()) + Number($('#mem2la').val()) + Number($('#mem2is').val()) + Number($('#mem2i').val());
      $('#mem2total').val(mem2total);
  });

  //interview total marks panel 3
  $( "#mem3vc" ).change(function() {
      var mem3total = Number($('#mem3vc').val()) + Number($('#mem3la').val()) + Number($('#mem3is').val()) + Number($('#mem3i').val());
      $('#mem3total').val(mem3total);
  });
  $( "#mem3la" ).change(function() {
      var mem3total = Number($('#mem3vc').val()) + Number($('#mem3la').val()) + Number($('#mem3is').val()) + Number($('#mem3i').val());
      $('#mem3total').val(mem3total);
  });
  $( "#mem3is" ).change(function() {
      var mem3total = Number($('#mem3vc').val()) + Number($('#mem3la').val()) + Number($('#mem3is').val()) + Number($('#mem3i').val());
      $('#mem3total').val(mem3total);
  });
  $( "#mem3i" ).change(function() {
      var mem3total = Number($('#mem3vc').val()) + Number($('#mem3la').val()) + Number($('#mem3is').val()) + Number($('#mem3i').val());
      $('#mem3total').val(mem3total);
  });

  //interview gd score average and final marks
  $( ".actions a" ).click(function() {
      var averageGD = (Number($('#mem1total').val()) + Number($('#mem2total').val()) + Number($('#mem3total').val()))/3;
      $('#average').val(averageGD.toFixed(2));
      var finalGD = (averageGD/20)*30;
      $('#final').val(finalGD.toFixed(2));
  });

  //scores for interview PI
  $( "#pimem1c" ).change(function() {
      var mem1total = Number($('#pimem1c').val()) + Number($('#pimem1d').val()) + Number($('#pimem1la').val()) + Number($('#pimem1cs').val());
      $('#pimem1total').val(mem1total);
  });
  $( "#pimem1d" ).change(function() {
    var mem1total = Number($('#pimem1c').val()) + Number($('#pimem1d').val()) + Number($('#pimem1la').val()) + Number($('#pimem1cs').val());
    $('#pimem1total').val(mem1total);
  });
  $( "#pimem1la" ).change(function() {
    var mem1total = Number($('#pimem1c').val()) + Number($('#pimem1d').val()) + Number($('#pimem1la').val()) + Number($('#pimem1cs').val());
    $('#pimem1total').val(mem1total);
  });
  $( "#pimem1cs" ).change(function() {
    var mem1total = Number($('#pimem1c').val()) + Number($('#pimem1d').val()) + Number($('#pimem1la').val()) + Number($('#pimem1cs').val());
    $('#pimem1total').val(mem1total);
  });

  //interview total marks panel 2
  //scores for interview PI
  $( "#pimem2c" ).change(function() {
      var mem2total = Number($('#pimem2c').val()) + Number($('#pimem2d').val()) + Number($('#pimem2la').val()) + Number($('#pimem2cs').val());
      $('#pimem2total').val(mem2total);
  });
  $( "#pimem2d" ).change(function() {
    var mem2total = Number($('#pimem2c').val()) + Number($('#pimem2d').val()) + Number($('#pimem2la').val()) + Number($('#pimem2cs').val());
    $('#pimem2total').val(mem2total);
  });
  $( "#pimem2la" ).change(function() {
    var mem2total = Number($('#pimem2c').val()) + Number($('#pimem2d').val()) + Number($('#pimem2la').val()) + Number($('#pimem2cs').val());
    $('#pimem2total').val(mem2total);
  });
  $( "#pimem2cs" ).change(function() {
    var mem2total = Number($('#pimem2c').val()) + Number($('#pimem2d').val()) + Number($('#pimem2la').val()) + Number($('#pimem2cs').val());
    $('#pimem2total').val(mem2total);
  });

  //interview total marks panel 3
  //scores for interview PI
  $( "#pimem3c" ).change(function() {
      var mem3total = Number($('#pimem3c').val()) + Number($('#pimem3d').val()) + Number($('#pimem3la').val()) + Number($('#pimem3cs').val());
      $('#pimem3total').val(mem3total);
  });
  $( "#pimem3d" ).change(function() {
    var mem3total = Number($('#pimem3c').val()) + Number($('#pimem3d').val()) + Number($('#pimem3la').val()) + Number($('#pimem3cs').val());
    $('#pimem3total').val(mem3total);
  });
  $( "#pimem3la" ).change(function() {
    var mem3total = Number($('#pimem3c').val()) + Number($('#pimem3d').val()) + Number($('#pimem3la').val()) + Number($('#pimem3cs').val());
    $('#pimem3total').val(mem3total);
  });
  $( "#pimem3cs" ).change(function() {
    var mem3total = Number($('#pimem3c').val()) + Number($('#pimem3d').val()) + Number($('#pimem3la').val()) + Number($('#pimem3cs').val());
    $('#pimem3total').val(mem3total);
  });

  //interview gd score average and final marks
  $( ".actions a" ).click(function() {
      var averageGD = (Number($('#pimem2total').val()) + Number($('#pimem3total').val()) + Number($('#pimem1total').val()))/3;
      $('#piaverage').val(averageGD.toFixed(2));
      var gd_skipped = $('#gd_skipped').val();
      console.log("gd_skipped"+gd_skipped);
      if(gd_skipped == "true"){
        var finalGD = (averageGD/20)*50;
      }else{
        var finalGD = (averageGD/20)*30;
      }
      $('#pifinal').val(finalGD.toFixed(2));
  });

  //interview-slected-check javascript
  $('#interview_select_profile').change(function() {
      if($(this).val() == "eds" || $(this).val() == "ens"){
        $('#interview_experience_salary_div').show();
      }else{
        $('#interview_experience_salary_div').hide();
        $('#interview_custom_pf_div').hide();
      }
  });

  $('#interview_salary_type_select').change(function() {
      if($(this).val() == "fdsc" || $(this).val() == "lumc" || $(this).val() == "gbc"){
        $('#interview_custom_pf_div').show();
      }else{
        $('#interview_custom_pf_div').hide();
      }
  });
//package custom select by anand
$('#package_custom_type_discount').hide();
$('#package_custom_type_flatprice').hide();
$('#package_custom_type').change(function() {
    if($(this).val() == "DI"){
      $('#package_custom_type_discount').show();
      $('#package_custom_type_flatprice').hide();
    }
    else{
      $('#package_custom_type_discount').hide();
      $('#package_custom_type_flatprice').show();

    }
});
//end package custom select by anand
  //requisition create -------
  $('#requisition_create_department_label').text($('#requisition_create_department').val());
  console.log($('#requisition_create_department').val());
  $('#requisition_create_position_label').text($('#requisition_create_position').val());
  $('#requisition_create_for_label').text($('#requisition_create_for').val());

  $('#requisition_create_requisition_name_label').text($('#requisition_create_requisition_name').val());
  $('#requisition_create_qualification_label').text($('#requisition_create_qualification').val());

  $('#requisition_create_department').change(function(){
    console.log($(this).val());

    $('#requisition_create_department_label').text($(this).val());
  });
  $('#requisition_create_position').change(function(){
    $('#requisition_create_position_label').text($(this).val());
  });
  $('#requisition_create_for').change(function(){
    $('#requisition_create_for_label').text($(this).val());
  });
  $('#requisition_create_number').change(function(){
    $('#requisition_create_number_label').text($(this).val());
  });
  $('#requisition_create_days').change(function(){
    $('#requisition_create_days_label').text($(this).val());
  });
  $('#requisition_create_replacement').change(function(){
    $('#requisition_create_replacement_label').text($(this).val());
  });
  $('#requisition_create_requisition_name').change(function(){
    $('#requisition_create_requisition_name_label').text($(this).val());
  });
  $('#requisition_create_client_name').change(function(){
    $('#requisition_create_client_name_label').text($(this).val());
  });
  $('#requisition_create_qualification').change(function(){
    $('#requisition_create_qualification_label').text($(this).val());
  });
  $('#requisition_create_experience').change(function(){
    $('#requisition_create_experience_label').text($(this).val());
  });
  $('#requisition_create_competencies').change(function(){
    $('#requisition_create_competencies_label').text($(this).val());
  });

});
