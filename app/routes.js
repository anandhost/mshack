const nodemailer = require("nodemailer");
const { TRANSPORTER_OPTIONS, SENDER } = require("../config/mailer");
const uniqueString = require('unique-string');
const moment = require('moment');
const pdf = require('dynamic-html-pdf');
const fs = require('fs');

const User = require('../app/models/user');
const Task = require('./models/task');
const Campus = require('./models/campus');
const Event = require('./models/event');
const Group = require('./models/group');
const Discussion = require('./models/discussion');

module.exports = function (app, passport) {

  //redirect to campus connect login page
  app.get('/', function (req, res) {
    if (req.isAuthenticated())
      return res.redirect('/dashboard');
    res.redirect('/login')
  });
  // permission page routing at campus connect
  app.use('/admin', isLoggedIn, isAdmin, require('./routes/admin'))

  app.use('/profile', isLoggedIn, require('./routes/profile'))

  app.use('/manager', isLoggedIn, isManager, require('./routes/manager'))

  app.use('/ambsdr', isLoggedIn, isAmbsdr, require('./routes/ambsdr'))
  //permission page redirection for campus connect
  app.get('/dashboard', isLoggedIn, function (req, res) {
    if (req.user.permission == "admin")
      res.redirect('/admin/dashboard');
    else if (req.user.permission == "manager")
      res.redirect('/manager/dashboard');
    else if (req.user.permission == "ambsdr")
      res.redirect('/ambsdr/dashboard');
    else {
      res.send("<center><h3>You don't have the user permissions to access the software yet.</h3><br> Please Mail us at manager@incubateind.com to request for the same. <a href='/logout'>Click Here</a> to go back.</center> ")
    }
  })
  //logout routing  to main login login page
  app.get('/logout', function (req, res) {
    req.session.destroy(function (err) {
      res.redirect('/');
    })
  });
  // CAMPUS CONNECT AUTHENTICATION - first login
  // show login form
  app.get('/login', function (req, res) {
    res.render('login.ejs', {
      messageError: req.flash('loginMessage'),
      messageSuccess: req.flash('loginSuccessMessage')
    });
  });
  // process the login form
  app.post('/login', passport.authenticate('local-login', {
    successRedirect: '/dashboard',
    failureRedirect: '/login',
    failureFlash: true
  }));
  // CAMPUS CONNECT SIGNUP
  // show the signup form
  app.get('/signup', function (req, res) {
    res.render('signup.ejs', { message: req.flash('signupMessage') });
  });

  // process the signup form
  app.post('/signup', passport.authenticate('local-signup', {
    successRedirect: '/dashboard',
    failureRedirect: '/signup',
    failureFlash: true
  }));

  app.get('/group/create', isLoggedIn, (req, res) => {
    res.render('group-create.ejs', {
      user : req.user,
      moment : moment
    })
  })

  app.post('/group/create', isLoggedIn, (req, res) => {
    let group = new Group({
      name : req.body.name,
      type : req.body.type,
      createdOn : Date.now(),
      admins: [req.user._id]
    });
    group.save((err) => {
      if (err) {
        res.json(err);
      }
      else {
        res.redirect('/group/' + group._id)
      }
    })
  })

  app.get('/my-groups/', isLoggedIn, (req, res) => {
    Group.find({$or : [{admins : req.user._id},{members : req.user._id}]}).sort({name : 1}).lean().exec((err, foundGroups) => {
      res.render('my-groups.ejs', {
        user : req.user,
        moment : moment,
        groups : foundGroups
      })
    })
  })

  app.get('/group/:id', isLoggedIn, (req, res) => {
    Group.findOne({_id : req.params.id}).lean().exec((err, foundGroup) => {
      Group.deepPopulate(foundGroup, ['admins','members','discussions'], (err, group) => {
        User.find({}).sort({name : 1}).lean().exec((err, foundUsers) => {
          res.render('group.ejs', {
            user : req.user,
            moment : moment,
            group : group,
            users : foundUsers
          })
        })
      })
    })
  })

  app.post('/group/member/add', isLoggedIn, (req, res) => {
    Group.updateOne({_id : req.body.group},{
      $addToSet : {
        members : req.body.member
      }
    }, (err) => {
      if (err) {
        res.json(err)
      }
      else {
        res.redirect('/group/' + req.body.group)
      }
    })
  })

  app.post('/discussion/add/:collection/:eventId', isLoggedIn, (req, res) => {
    let discussion = new Discussion();
    discussion.post = req.body.post;
    //Image functionality here
    discussion.save((err, result) => {
      if (err) {
        res.json(err);
      }
      else {
        if (req.params.collection == 'group') {
          Group.updateOne({ _id: req.params.eventId }, {
            $push: {
              discussions: discussion._id
            }
          }, (err) => {
            if (err) {
              res.json(err);
            }
            else {
              res.redirect(req.header('referrer'));
            }
          })
        }
        else {
          Event.updateOne({ _id: req.params.eventId }, {
            $push: {
              discussions: discussion._id
            }
          }, (err) => {
            if (err) {
              res.json(err);
            }
            else {
              res.redirect(req.header('referrer'));
            }
          })
        }
      }
    })
  })

  app.post('/comment/add/:discussionId', isLoggedIn, (req, res) => {
    Discussion.updateOne({ _id: req.params.discussionId }, {
      $addToSet: {
        comments: {
          comment: req.body.comment,
          commentedBy: req.user._id,
          createdAt: Date.now()
        }
      }
    }, (err) => {
      if (err) {
        res.json(err);
      }
      else {
        res.redirect(req.header('referrer'));
      }
    })
  })

}; //module.exports end here

//routes middleware for loggedin users
function isLoggedIn(req, res, next) {
  if (req.isAuthenticated())
    return next();

  res.redirect('/');
}
function isAdmin(req, res, next) {
  if (req.user.permission == "admin")
    return next();

  else {
    res.send("Not permitted");
  }
}

function isManager(req, res, next) {
  if (req.user.permission == "manager" || req.user.permission == "admin")
    return next();

  else {
    res.send("Not permitted");
  }
}

function isAmbsdr(req, res, next) {
  if (req.user.permission == "ambsdr" || req.user.permission == "admin")
    return next();

  else {
    res.send("Not permitted");
  }
}
// function isLinked(req, res, next)
// {
//   if(req.user.emp)
//   return next();
//
//   else {
//     res.send("Your Account is not linked with your manager yet. Please contact your manager");
//   }
// }
