var mongoose = require('mongoose')
var Schema = mongoose.Schema

const deepPopulate = require('mongoose-deep-populate')(mongoose)
//= ===test Schema====
var taskSchema = new Schema({
    title: String,
    status: Number,
    createdAt: Date,
    description: String,
    dueDate: Date,
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    assignedTo: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
})

taskSchema.plugin(deepPopulate)
module.exports = mongoose.model('Task', taskSchema)