var mongoose = require('mongoose')
var Schema = mongoose.Schema
const deepPopulate = require('mongoose-deep-populate')(mongoose)
//= ===test Schema====
var groupSchema = new Schema({
  name : String,
  createdOn : Date,
  admins: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
  }],
  members: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
  }],
  discussions: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Discussion'
  }],
})

groupSchema.plugin(deepPopulate)
module.exports = mongoose.model('Group', groupSchema)
