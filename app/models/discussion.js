var mongoose = require('mongoose')
var Schema = mongoose.Schema
const deepPopulate = require('mongoose-deep-populate')(mongoose)
//= ===test Schema====
var discussionSchema = new Schema({
    post: String,
    image: [String],
    comments: [{
        comment: String,
        commentedBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        createdAt: Date
    }]
})

discussionSchema.plugin(deepPopulate)
module.exports = mongoose.model('Discussion', discussionSchema)