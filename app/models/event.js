var mongoose = require('mongoose')
var Schema = mongoose.Schema

const deepPopulate = require('mongoose-deep-populate')(mongoose)
//= ===test Schema====
var eventSchema = new Schema({
    name: String,
    location: {
        addressLine: String,
        region: String,
        pincode: Number
    },
    campus: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Campus'
    },
    createdAt: Date,
    description: String,
    EventDate: Date,
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },

    tasks: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Task'
    }],
    discussions: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Discussion'
    }]
})

eventSchema.plugin(deepPopulate)
// Question model
module.exports = mongoose.model('Event', eventSchema)
