var mongoose = require('mongoose')
var Schema = mongoose.Schema
const deepPopulate = require('mongoose-deep-populate')(mongoose)
//= ===test Schema====
var campusSchema = new Schema({
    name: String,
    region: String,
    location: {
        addressLine: String,
        region: String,
        pincode: Number
    },
    ambassadors: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    phone: Number,
    email: String,
})
campusSchema.plugin(deepPopulate)
module.exports = mongoose.model('Campus', campusSchema)
