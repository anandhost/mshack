var app = require('express').Router();
var User = require('../models/user');

app.get('/', function(req, res) {
    res.render('profile.ejs', {
        user : req.user
    });
});

app.get('/edit', function(req, res) {
    res.render('profile-edit.ejs', {
        user : req.user,
        detailError : req.flash('profileDetailError'),
        detailSuccess : req.flash('profileDetailSuccess'),
        photoError : req.flash('profilePhotoError'),
        photoSuccess : req.flash('profilePhotoSuccess')
    });
});

app.post('/edit/details', function(req, res) {
  User.updateOne({username : req.body.username},{
    $set : {
      name : req.body.name,
      email : req.body.email,
      number : req.body.number,
      location : req.body.location
    }},
    function(err, result){
      if(err) {
        console.log(err);
        req.flash('profileDetailError','An error occured. Please try again!');
        res.redirect('/profile/edit');
      }
      else {
        req.flash('profileDetailSuccess','Your details were changed successfully!');
        res.redirect('/profile/edit');
      }
    })
});

app.post('/edit/photo', function(req, res) {
  req.files.profilePhoto.mv('./public/profilePhoto/'+ req.body.id + '.png');
  User.updateOne({_id : req.body.id},{
    $set : {
      profilePhoto : req.body.id + ".png"
    }},
    function(err, result){
      if(err) {
        console.log(err);
        req.flash('profilePhotoError','An error occured. Please try again!');
        res.redirect('/profile/edit');
      }
      else {
        req.flash('profilePhotoSuccess','Your profile photo was changed successfully!');
        res.redirect('/profile/edit');
      }
    })
});

module.exports = app;
