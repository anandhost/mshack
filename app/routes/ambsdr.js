const app = require('express').Router();
const moment = require('moment');
const User = require('../models/user');
const Task = require('../models/task');


app.get('/dashboard', (req, res) => {
    res.render('ambsdr/dashboard.ejs', {
        user: req.user,
    });
});

app.get('/previoustask', (req, res) => {
    Task.find({
        assignedTo: req.user.id,
        "dueDate": { $lt: new Date() }
    }).then(tasks => {
        console.log(tasks)
        res.render('ambsdr/previousTasks.ejs', {
            user: req.user,
            tasks
        })
    })
})


app.get('/report', (req, res) => {
    res.render('ambsdr/dashboard.ejs', {
        user: req.user
    })
})

app.get('/mytask', (req, res) => {
    Task.find({
        assignedTo: req.user.id
    }).then(tasks => {
        console.log(tasks)
        res.render('ambsdr/mytask.ejs', {
            user: req.user,
            tasks
        })
    })
})
module.exports = app;
