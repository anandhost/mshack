const app = require('express').Router();
const moment = require('moment');
const User = require('../models/user');
const Task = require('../models/task');
const Campus = require('../models/campus');
const Event = require('../models/event');
const util = require('util')

app.get('/dashboard', (req, res) => {
    res.render('manager/dashboard.ejs', {
        user: req.user,
    });
});
app.get('/viewEvents', (req, res) => {
    Event.find({}).lean().then((eventList) => {
        Event.deepPopulate(eventList, ['discussions', 'campus', 'createdBy', 'discussions.comments.commentedBy'], function (err, docs) {
            console.log('JSON.stringify(docs)', console.log(util.inspect(docs, { showHidden: false, depth: null })))
            res.render('manager/viewEvents.ejs', {
                user: req.user,
                moment: moment,
                events: docs
            });
        })
        // res.json({ docs })
    })
});
app.get('/viewEvents/details', (req, res) => {
    Event.find({}).lean().then((eventList) => {
        Event.deepPopulate(eventList, ['discussions', 'campus', 'createdBy', 'discussions.comments.commentedBy'], function (err, docs) {
            console.log('JSON.stringify(docs)', console.log(util.inspect(docs, { showHidden: false, depth: null })))
            res.render('manager/viewEvents-details.ejs', {
                user: req.user,
                moment: moment,
                events: docs
            });
        })
        // res.json({ docs })
    })
});
app.get('/upcomingEvents', (req, res) => {
    // Event.find({ "EventDate": { $gt: Date.now() } }, function (err, docs) {
    Event.find({ "EventDate": { $gt: Date.now() } }).lean().then((eventList) => {
        Event.deepPopulate(eventList, ['discussions', 'campus', 'createdBy', 'discussions.comments.commentedBy'], function (err, docs) {
            console.log('JSON.stringify(docs)', console.log(util.inspect(docs, { showHidden: false, depth: null })))
            res.render('manager/viewEvents-details.ejs', {
                user: req.user,
                moment: moment,
                events: docs
            });
        })
        // res.json({ docs })
    })

});
app.get('/pastEvents', (req, res) => {
    Event.find({ "EventDate": { $lt: new Date() } }).lean().then((eventList) => {
        Event.deepPopulate(eventList, ['discussions', 'campus', 'createdBy', 'discussions.comments.commentedBy'], function (err, docs) {
            console.log('JSON.stringify(docs)', console.log(util.inspect(docs, { showHidden: false, depth: null })))
            res.render('manager/viewEvents-details.ejs', {
                user: req.user,
                moment: moment,
                events: docs
            });
        })
        // res.json({ docs })
    })

});
app.get('/addEvents', (req, res) => {
    Campus.find({}).then(campuses => {
        res.render('manager/addEvents.ejs', {
            user: req.user,
            campuses,
            script: false
        });
    })
});
app.get('/addCampus', (req, res) => {
    res.render('manager/addCampus.ejs', {
        user: req.user,
        obj: undefined
    });
});
app.post('/addCampus', (req, res) => {
    let campus = new Campus({
        name: req.body.name,
        // region: req.body.region,
        location: {
            region: req.body.region,
            addressLine: req.body.addressLine,
            pincode: req.body.pincode
        },
        phone: req.body.phone,
        email: req.body.email,
    })

    campus.save().then(obj => res.render("manager/addCampus", { user: req.user, obj }))
});

app.get('/viewCampus', (req, res) => {
    // res.json({})
    console.log(req.user)
    Campus.find({ 'location.region': req.user.region }).then(result => {
        Campus.deepPopulate(result, 'ambassadors', function (err, docs) {
            User.find({ 'region': req.user.region, permission: "ambsdr" }).then(ambsdr => {
                console.log("docsdocsdocsdocsdocs", JSON.stringify(docs))
                console.log("ambsdr", ambsdr)
                console.log("++++++++++++++++++++++++++++++++++++++++++++\n\n campuses", docs, "---user", req.user, "-----ambsdr", ambsdr, "\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~");
                res.render('manager/viewCampus', {
                    campuses: docs,
                    user: req.user,
                    obj: {},
                    ambsdr

                })
            })
        })
        // res.json(result)
    })
})

app.post('/ambassador/add', (req, res) => {
    Campus.updateOne({ _id: req.body.campus }, {
        $addToSet: {
            ambassadors: req.body.ambassdor
        }
    }, (err) => {
        if (err) {
            res.json(err)
        }
        else {
            res.redirect('/manager/viewCampus');
        }
    })
})

app.post('/addEvents', (req, res) => {
    console.log("req.user------", req.user, "req.body-------", req.body);
    // res.json(req.body);
    event = new Event({
        name: req.body.name,
        createdAt: Date.now(),
        description: req.body.description,
        EventDate: req.body.EventDate,
        createdBy: req.user._id,
    })
    if (req.body.campus && req.body.campus.length > 0) {
        event.campus = req.body.campus
    }
    else {
        event.location = {
            region: req.body.region,
            addressLine: req.body.addressLine,
            pincode: req.body.pincode
        }
    }
    event.save().then(obj => {
        console.log("rendering ", obj)
        Campus.find({}).then(campuses => {
            res.render('manager/addEvents.ejs', {
                user: req.user,
                campuses,
                script: true
            });
        })
    })
    // res.render('manager/addEvents.ejs', {
    //     user: req.user,
    // });
});

app.post('/addTask/:id', (req, res) => {
    console.log("req.user------", req.user, "req.body-------", req.body, "params.id", req.params.id);
    // res.json(req.body);

    task = new Task({
        name: req.body.name,
        title: req.body.title,
        createdAt: Date.now(),
        description: req.body.description,
        dueDate: req.body.dueDate,
        assignedTo: req.params.id,
        createdBy: req.user._id,
        status: 0,
    })

    task.save().then(obj => {
        console.log("rendering ", obj)
        Campus.find({}).then(taskAdded => {
            // res.json(taskAdded)
            console.log(taskAdded)
            User.findOne({
                _id: req.params.id
            }).then(foundUser => {
                console.log({ foundUser });
                Task.find({ assignedTo: foundUser, createdBy: req.user._id }).then(data => {
                    // res.json(data)
                    res.render('manager/tasks', {
                        foundUser: foundUser,
                        user: req.user,
                        tasks: data
                    })
                    // res.send(req.params.id);
                })
            })
            // res.render('manager/addEvents.ejs', {
            //     user: req.user,
            //     campuses,
            //     script: true
            // });
        })
    })
    // res.render('manager/addEvents.ejs', {
    //     user: req.user,
    // });
});

app.get('/approve-permission', (req, res) => {
    User.find({ tempPermission: "ambsdr" }).lean().exec((err, foundUsers) => {
        res.render('manager/approve-permission.ejs', {
            user: req.user,
            moment: moment,
            users: foundUsers
        })
    })
})

app.get('/approve-permission/approve/:id', (req, res) => {
    User.updateOne({ _id: req.params.id }, {
        $set: {
            permission: "ambsdr"
        },
        $unset: {
            tempPermission: true
        }
    }, (err, result) => {
        if (err) {
            res.json(err)
        }
        else {
            res.redirect('/manager/approve-permission');
        }
    })
})

app.get('/approve-permission/reject/:id', (req, res) => {
    User.deleteOne({ _id: req.params.id }, (err, foundUser) => {
        if (err) {
            res.json(err)
        }
        else {
            res.redirect('/manager/approve-permission');
        }
    })
})

app.get("/ambassdor/:id", (req, res) => {
    User.findOne({
        _id: req.params.id
    }).then(foundUser => {
        console.log({ foundUser });
        Task.find({ assignedTo: foundUser, createdBy: req.user._id }).then(data => {
            // res.json(data)
            res.render('manager/tasks', {
                foundUser: foundUser,
                user: req.user,
                tasks: data
            })
            // res.send(req.params.id);
        })
    })
});

module.exports = app;
