const app = require('express').Router();
const request = require('request');
const moment = require('moment');

const User = require('../models/user');


app.get('/dashboard', (req, res)=> {
  res.render('admin/dashboard.ejs', {
    user : req.user,
    moment : moment
  });
});

//NEW USER ACCOUNT CREATION

app.get('/user/create', (req, res) => {
  res.render('admin/user-create.ejs', {
    user : req.user,
    moment : moment,
    errorMessage : req.flash("errorMessage"),
    successMessage : req.flash("successMessage")
  });
})

app.post('/user/create', (req, res) => {
  User.findOne({ 'username' :  req.body.username }, function(err, user) {
    // if there are any errors, return the error
    if (err)
    return done(err);
    // check to see if theres already a user with that username
    if (user) {
      return done(null, false, req.flash('signupMessage', 'The username is already taken.'));
    } else {

      User.findOne({ 'email' :  req.body.email }, function(err, emailid) {

        if (err) {
          console.log("Error : " + err);
          req.flash('errorMessage', 'An Error Occured! Please Try Again.');
          res.redirect("/admin/user/create");
        }

        if (emailid) {
          req.flash('errorMessage', 'The Email Id already exists.');
          res.redirect("/admin/user/create");
        } else {

        // create the user
        var newUser = new User();

        newUser.username = req.body.username;
        newUser.password = newUser.generateHash(req.body.password);
        newUser.permission = req.body.permission;
        newUser.region = req.body.region;
        newUser.email = req.body.email;
        newUser.name = req.body.name;
        newUser.profilePhoto = "default.png";

        newUser.save(function(err) {
          if (err) {
            console.log("Error : " + err);
            req.flash('errorMessage', 'An Error Occured! Please Try Again.');
            res.redirect("/admin/user/create");
          }
          else {
            req.flash('successMessage','User created successfully! The account is ready to use.')
            res.redirect('/admin/user/create');
          }
        });
      }
      });
    }
  });
})

//USER PERMISSION APPROVAL

app.get('/approve-permission', (req, res) => {
  User.find({tempPermission : {$exists : true}}).sort({tempPermission : -1}).lean().exec((err, foundUsers) => {
    res.render('admin/approve-permission.ejs', {
      user : req.user,
      moment : moment,
      users : foundUsers
    })
  })
})

app.get('/approve-permission/approve/:id/:permission', (req, res) => {
  User.updateOne({_id : req.params.id}, {
    $set : {
      permission : req.params.permission
    },
    $unset : {
      tempPermission : true
    }
  }, (err, result) => {
    if (err) {
      res.json(err)
    }
    else {
      res.redirect('/admin/approve-permission');
    }
  })
})

app.get('/approve-permission/reject/:id', (req, res) => {
  User.deleteOne({_id : req.params.id}, (err, foundUser) => {
    if (err) {
      res.json(err)
    }
    else {
      res.redirect('/admin/approve-permission');
    }
  })
})

//VIEW MANAGER TABLE

app.get('/manager/view', (req, res) => {
  User.find({permission : "manager"}).sort({name : 1}).lean().exec((err, foundManagers) => {
    res.render('admin/manager-table.ejs', {
      user : req.user,
      moment : moment,
      managers : foundManagers
    })
  })
})

module.exports = app;
